angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
})

.controller('EstablishmentsCtrl', ['$scope','baseURL', 'establishments',
                               function ($scope, baseURL, establishments) {
    $scope.baseURL = baseURL;

    $scope.establishments = establishments;

    $scope.maxRating = 5;
    $scope.readOnly = true;

    $scope.inputfill = "";

    // Filtering options
    $scope.hasBadges = false;
    $scope.criteria = '-rate';
    $scope.distance = 20;

    $scope.messages = 0;
    $scope.requests = 0;

    $scope.tabsFilter = function(element) {
        if(($scope.hasBadges && element.label === "") ||
            ($scope.distance < element.distance)) {
            return false;
        }
        return true;
    };

}])

;
